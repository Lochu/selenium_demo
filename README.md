###1. Stosowanie WebDriverManager.chromedriver().setup() zamiast System.setProperty("webdriver.chrome.driver", ".\\Driver\\chromedriver.exe");

WebDriverManager.chromedriver().setup: checks for the latest version of the specified WebDriver binary.
If the binaries are not present on the machine, then it will download the WebDriver binaries.
Next, it instantiates the Selenium WebDriver instance with the ChromeDriver.

###2. Używanie unikalnych id (w miarę możliwości). Jeśli jest możliwość zawsze szukaj selektora po ID. Uważać na lokalizowanie
   po klasach szczególnie jeśli widzisz, że klasa jest generowana automatycznie przez framework.
   Kilka przykładowych xpath które najczęściej są używane:
- //*[contains(@yourAttribute, 'yourAttributeValue')]   --> //*[contains(@id, 'partOfYourId')]
- //*[text()='yourText']
- //*[contains(text(), 'yourText')]

Sposoby wyszukiwania elementów za pomocą xpath/css:
[Most Exhaustive WebDriver Locators Cheat Sheet](https://www.automatetheplanet.com/selenium-webdriver-locators-cheat-sheet/?fbclid=IwAR0YcUdA0UtwcxVLcqg_cBJRQOdYlb1Ep3br2sV_gF9F6LyyxVsRspsLf_s)

(Opcjonalnie) Trzymanie selektorów w interfejsach. Przede wszystkim takie rozwiąznie zwiększa nam czytelność Pages
- deklaracja selektorów typu By,
- na końcu każdej nazwy selektora dodajemy końcówkę "Selector".

###3. Awaitility zamiast explicit/implicit waitów z Selenium --> przykłady w Interactions class.

###Dodatkowe pomysły aby usprawnić/przyśpieszyć/polepszyć testy automatyczne w selenium:
- stosowanie //given //when //then aby polepszyć czytelność testów
- stosowanie wzorca projektowego builder jeśli często budujemy te same obiekty,
- stosowanie REST ASSURED aby przyśpieszyć testy które zostały już przetestowane end-to-end,
- namówienie swoich FE developerów żeby pisali testy jednostkowe (będą używać tych samych selektorów co Ty, a jeśli
  ich nie ma to będą zmuszeni je zrobić).