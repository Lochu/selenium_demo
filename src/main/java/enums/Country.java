package enums;

import lombok.Getter;

@Getter
public enum Country {
    UNITED_STATES("United States");

    private final String value;

    Country(String value) {
        this.value = value;
    }
}
