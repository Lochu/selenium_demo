package enums;

import lombok.Getter;

@Getter
public enum State {
    ALABAMA("Alabama"),
    ALASKA("Alaska"),
    ARIZONA("Arizona"),
    ARKANSAS("Arkansas"),
    CALIFORNIA("California"),
    COLORADO("Colorado");

    private final String value;

    State(String value) {
        this.value = value;
    }
}
