package objects;

import enums.Country;
import enums.State;
import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class AddressInformation {
    String companyName;
    String addressDetails;
    String cityName;
    State state;
    int postalCode;
    Country country;
    int mobilePhone;
    String emailAddressForFutureReference;

    public static AddressInformation sample() {
        return AddressInformation.builder()
                .companyName("Testing Company")
                .addressDetails("Testing Street 29F")
                .cityName("Sosnowiec")
                .state(State.CALIFORNIA)
                .postalCode(20433)
                .country(Country.UNITED_STATES)
                .mobilePhone(501374982)
                .emailAddressForFutureReference("testingEmail@test.com")
                .build();
    }
}
