package objects;

import enums.MonthOfBirth;
import enums.Title;
import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class PersonalInformation {
    Title title;
    String firstName;
    String lastName;
    String password;
    MonthOfBirth monthOfBirth;
    int dayOfBirth;
    int yearOfBirth;

    public static PersonalInformation sample() {
        return PersonalInformation.builder()
                .title(Title.MR)
                .firstName("Levi")
                .lastName("Ackerman")
                .password("SecretSauce999")
                .monthOfBirth(MonthOfBirth.APRIL)
                .dayOfBirth(2)
                .yearOfBirth(1995)
                .build();
    }
}
