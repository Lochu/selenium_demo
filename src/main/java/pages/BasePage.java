package pages;

import org.assertj.core.api.Assertions;
import org.openqa.selenium.WebDriver;
import selectors.BasePageSelectors;

import static utlis.Interactions.awaitUntilElementsAreDisplayed;

public abstract class BasePage implements BasePageSelectors {
    protected WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public void checkPriceOfExistClothesAreMoreExpensiveThenZeroDollars() {
        awaitUntilElementsAreDisplayed(driver, allClothPriceSelector);
        driver.findElements(allClothPriceSelector).stream()
                .map(e -> e.getText().replace("$", "").strip())
                .map(Double::parseDouble)
                .forEach(e -> Assertions.assertThat(e).isGreaterThan(0));
    }
}
