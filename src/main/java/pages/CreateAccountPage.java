package pages;

import enums.Title;
import objects.AddressInformation;
import objects.PersonalInformation;
import org.openqa.selenium.WebDriver;
import selectors.CreateAccountPageSelectors;

import static utlis.Interactions.*;

public class CreateAccountPage extends BasePage implements CreateAccountPageSelectors {

    public CreateAccountPage(WebDriver driver) {
        super(driver);
    }

    public void completePersonalInformation(PersonalInformation personalInformation) {
        click(driver, personalInformation.getTitle() == Title.MR ? mrCheckboxSelector : mrsCheckboxSelector);
        sendKeys(driver, firstNameInputSelector, personalInformation.getFirstName());
        sendKeys(driver, lastNameInputSelector, personalInformation.getLastName());
        sendKeys(driver, passwordInputSelector, personalInformation.getPassword());
        click(driver, dayOfBirthDropdownSelector);
        click(driver, dayOfBirthInDropdownSelector(personalInformation));
        click(driver, monthOfBirthDropdownSelector);
        click(driver, monthOfBirthInDropdownSelector(personalInformation));
        click(driver, yearOfBirthDropdownSelector);
        click(driver, yearOfBirthInDropdownSelector(personalInformation));
    }

    public void completeAddressInformation(AddressInformation addressInformation) {
        sendKeys(driver, companyNameInputSelector, addressInformation.getCompanyName());
        sendKeys(driver, addressDetailsInputSelector, addressInformation.getAddressDetails());
        sendKeys(driver, cityNameInputSelector, addressInformation.getCityName());
        click(driver, stateDropdownSelector);
        click(driver, stateInDropdownSelector(addressInformation));
        sendKeys(driver, postalCodeInputSelector, String.valueOf(addressInformation.getPostalCode()));
        click(driver, countryDropdownSelector);
        click(driver, countryInDropdownSelector(addressInformation));
        sendKeys(driver, mobilePhoneInputSelector, String.valueOf(addressInformation.getMobilePhone()));
        cleanFieldAndSendKeys(driver, emailAddressForFutureReferenceInputSelector, addressInformation.getEmailAddressForFutureReference());
    }

    public MyAccountPage goToMyAccountPage() {
        click(driver, registerButtonSelector);
        return new MyAccountPage(driver);
    }
}
