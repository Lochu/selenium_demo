package pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import selectors.MainPageSelectors;

import static utlis.Interactions.*;

public class MainPage extends BasePage implements MainPageSelectors {

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public WomenPage goToWomanPage() {
        click(driver, womenPageSelector);
        return new WomenPage(driver);
    }

    public void searchByName(String itemName) {
        sendKeys(driver, searchEngineInputSelector, itemName);
        click(driver, searchEngineButtonSelector);
    }

    public void assertOnlyOneElementIsExist() {
        try {
            awaitUntilElementIsDisplayed(driver, assertionOneOfOneItemSelector);
        } catch (Exception e) {
            throw new NoSuchElementException("Found more then one or less then one item");
        }
    }

    public SignInPage goToSignInPage() {
        click(driver, signInPageSelector);
        return new SignInPage(driver);
    }
}
