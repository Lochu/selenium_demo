package pages;

import org.openqa.selenium.WebDriver;
import selectors.MyAccountPageSelectors;

import static utlis.Interactions.awaitUntilElementIsDisplayed;

public class MyAccountPage extends BasePage implements MyAccountPageSelectors {
    public MyAccountPage(WebDriver driver) {
        super(driver);
    }

    public void assertPresentInMyAccountPage() {
        awaitUntilElementIsDisplayed(driver, myAccountPageSelector);
    }
}
