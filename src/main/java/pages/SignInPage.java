package pages;

import org.openqa.selenium.WebDriver;
import selectors.SignInPageSelectors;

import java.util.Random;

import static utlis.Interactions.*;

public class SignInPage extends BasePage implements SignInPageSelectors {
    public SignInPage(WebDriver driver) {
        super(driver);
    }

    private String createRandomEmail() {
        final String USERNAME = "username";
        final String GMAIL = "@gmail.com";
        Random randomGenerator = new Random();
        final int randomInt = randomGenerator.nextInt(100000);
        return USERNAME + randomInt + GMAIL;
    }

    public void completeEmailAddress() {
        sendKeys(driver, emailAddressInputSelector, createRandomEmail());
    }

    public CreateAccountPage goToCreateAccountPage() {
        click(driver, createAnAccountButtonSelector);
        return new CreateAccountPage(driver);
    }
}

