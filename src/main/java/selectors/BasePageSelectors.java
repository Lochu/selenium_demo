package selectors;

import org.openqa.selenium.By;

public interface BasePageSelectors {
    By allClothPriceSelector = By.xpath("//div[@class='right-block']//span[@class='price product-price']");
}
