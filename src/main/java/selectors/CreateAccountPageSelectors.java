package selectors;

import objects.AddressInformation;
import objects.PersonalInformation;
import org.openqa.selenium.By;

public interface CreateAccountPageSelectors {
    By mrCheckboxSelector = By.id("id_gender1");
    By mrsCheckboxSelector = By.id("id_gender2");
    By firstNameInputSelector = By.id("customer_firstname");
    By lastNameInputSelector = By.id("customer_lastname");
    By passwordInputSelector = By.id("passwd");
    By dayOfBirthDropdownSelector = By.id("uniform-days");
    By monthOfBirthDropdownSelector = By.id("uniform-months");
    By yearOfBirthDropdownSelector = By.id("uniform-years");
    By companyNameInputSelector = By.id("company");
    By addressDetailsInputSelector = By.id("address1");
    By cityNameInputSelector = By.id("city");
    By stateDropdownSelector = By.id("uniform-id_state");
    By countryDropdownSelector = By.id("uniform-id_country");
    By postalCodeInputSelector = By.id("postcode");
    By mobilePhoneInputSelector = By.id("phone_mobile");
    By emailAddressForFutureReferenceInputSelector = By.id("alias");
    By registerButtonSelector = By.id("submitAccount");

    default By dayOfBirthInDropdownSelector(PersonalInformation personalInformation) {
        return By.xpath("//*[@id='days']//*[@value=" + personalInformation.getDayOfBirth() + "]");
    }

    default By monthOfBirthInDropdownSelector(PersonalInformation personalInformation) {
        return By.xpath("//*[@id='months']//*[contains(text(), '" + personalInformation.getMonthOfBirth().getValue() + "')]");
    }

    default By yearOfBirthInDropdownSelector(PersonalInformation personalInformation) {
        return By.xpath("//*[@id='years']//*[@value=" + personalInformation.getYearOfBirth() + "]");
    }

    default By stateInDropdownSelector(AddressInformation addressInformation) {
        return By.xpath("//*[@id='id_state']//*[contains(text(), '" + addressInformation.getState().getValue() + "')]");
    }

    default By countryInDropdownSelector(AddressInformation addressInformation) {
        return By.xpath("//*[@id='id_country']//*[contains(text(), '" + addressInformation.getCountry().getValue() + "')]");
    }
}
