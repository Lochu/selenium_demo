package selectors;

import org.openqa.selenium.By;

public interface MainPageSelectors {
    By womenPageSelector = By.className("sf-with-ul");
    By searchEngineInputSelector = By.id("search_query_top");
    By searchEngineButtonSelector = By.name("submit_search");
    By assertionOneOfOneItemSelector = By.xpath("//*[@class='top-pagination-content clearfix']//div[contains(text(), 'Showing 1 - 1 of 1 item')]");
    By signInPageSelector = By.className("header_user_info");
}
