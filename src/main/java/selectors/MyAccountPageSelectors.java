package selectors;

import org.openqa.selenium.By;

public interface MyAccountPageSelectors {
    By myAccountPageSelector = By.xpath("//span[contains(text(), 'My account')]");
}
