package selectors;

import org.openqa.selenium.By;

public interface SignInPageSelectors {
    By createAnAccountButtonSelector = By.id("SubmitCreate");
    By emailAddressInputSelector = By.id("email_create");
}
