package utlis;

import org.awaitility.Awaitility;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class Interactions {
    private final static int TIMEOUT = 10;

    public static void click(WebDriver driver, By selector) {
        awaitUntilElementIsDisplayed(driver, selector);
        driver.findElement(selector).click();
    }

    public static void awaitUntilElementIsDisplayed(WebDriver driver, By selector) {
        Awaitility.await().atMost(TIMEOUT, TimeUnit.SECONDS).ignoreExceptions().until(() -> driver.findElement(selector).isDisplayed());
    }

    public static void awaitUntilElementsAreDisplayed(WebDriver driver, By selector) {
        Awaitility.await().atMost(TIMEOUT, TimeUnit.SECONDS).ignoreExceptions().until(() -> driver.findElements(selector).size() > 0);
    }

    public static void sendKeys(WebDriver driver, By selector, String keys) {
        click(driver, selector);
        driver.findElement(selector).sendKeys(keys);
    }

    public static void cleanFieldAndSendKeys(WebDriver driver, By selector, String keys) {
        awaitUntilElementIsDisplayed(driver, selector);
        driver.findElement(selector).clear();
        sendKeys(driver, selector, keys);
    }
}
