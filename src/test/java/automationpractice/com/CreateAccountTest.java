package automationpractice.com;

import objects.AddressInformation;
import objects.PersonalInformation;
import org.junit.jupiter.api.Test;
import pages.BaseTest;
import pages.CreateAccountPage;
import pages.MyAccountPage;
import pages.SignInPage;

public class CreateAccountTest extends BaseTest {
    private SignInPage signInPage;
    private CreateAccountPage createAccountPage;
    private MyAccountPage myAccountPage;

    @Test
    public void createUserAccount() {
        //given
        PersonalInformation personalInformation = PersonalInformation.sample();
        AddressInformation addressInformation = AddressInformation.sample();

        //when
        signInPage = mainPage.goToSignInPage();
        signInPage.completeEmailAddress();
        createAccountPage = signInPage.goToCreateAccountPage();

        //and
        createAccountPage.completePersonalInformation(personalInformation);
        createAccountPage.completeAddressInformation(addressInformation);
        myAccountPage = createAccountPage.goToMyAccountPage();

        //then
        myAccountPage.assertPresentInMyAccountPage();
    }
}
