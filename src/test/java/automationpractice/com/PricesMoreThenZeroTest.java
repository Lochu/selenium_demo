package automationpractice.com;

import org.junit.jupiter.api.Test;
import pages.BaseTest;
import pages.WomenPage;

public class PricesMoreThenZeroTest extends BaseTest {
    private WomenPage womanPage;

    @Test
    public void assertAllClothesPricesAreMoreThenZeroDollars() {
        //when
        womanPage = mainPage.goToWomanPage();

        //then
        womanPage.checkPriceOfExistClothesAreMoreExpensiveThenZeroDollars();
    }
}
