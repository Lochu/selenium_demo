package automationpractice.com;

import org.junit.jupiter.api.Test;
import pages.BaseTest;

public class SearchEngineTest extends BaseTest {

    @Test
    public void checkSearchEngineFilterOnlyBlouseWithPositivePrice() {
        //given
        final String itemName = "blouse";

        //when
        mainPage.searchByName(itemName);

        //then
        mainPage.assertOnlyOneElementIsExist();
        mainPage.checkPriceOfExistClothesAreMoreExpensiveThenZeroDollars();
    }
}
